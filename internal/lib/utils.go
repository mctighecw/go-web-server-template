package lib

import (
	"fmt"
	"math/rand"
	"os"

	"github.com/labstack/echo/v4"
)

func LogError(err error) {
	if err != nil {
		fmt.Println(err)
		return
	}
}

func ErrorMessage(message string) interface{} {
	res := map[string]interface{}{
		"status":  "Error",
		"message": message,
	}
	return res
}

func GetEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func MakeRandomString(n int) string {
	const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	b := make([]byte, n)

	for i := range b {
		b[i] = characters[rand.Int63()%int64(len(characters))]
	}
	return string(b)
}

func GetEchoMap(c echo.Context) echo.Map {
	m := echo.Map{}
	if err := c.Bind(&m); err != nil {
		LogError(err)
	}
	return m
}

func ReturnStringValue(m echo.Map, field string) string {
	v := fmt.Sprintf("%v", m[field])
	return v
}

func ReturnBoolValue(m echo.Map, field string) bool {
	a := m[field]
	var b bool = bool(a.(bool))
	return b
}

func If[T any](cond bool, vTrue, vFalse T) T {
	if cond {
		return vTrue
	}
	return vFalse
}
