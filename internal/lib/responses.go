package lib

func ReturnTokenOK(m string, t string) interface{} {
	r := map[string]interface{}{"status": "OK", "message": m, "token": t}
	return r
}

func ReturnError(m string) interface{} {
	r := map[string]interface{}{"status": "Error", "message": m}
	return r
}

func ReturnOK(m string) interface{} {
	r := map[string]interface{}{"status": "OK", "message": m}
	return r
}
