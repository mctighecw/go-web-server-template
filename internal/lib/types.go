package lib

type MessageResponse struct {
	Message string `json:"message"`
}

type StatusResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type UserData struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	Username  string `json:"username"`
	Admin     bool   `json:"admin"`
	CreatedAt string `json:"created_at"`
}
