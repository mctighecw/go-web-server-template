package config

import (
	"os"

	"go-web-server/internal/lib"
)

type (
	App struct {
		Name string
		Port uint
	}

	DB struct {
		Host     string
		Port     uint
		User     string
		Password string
		AdminDb  string
		DbName   string
	}
)

var (
	APP_CONFIG *App
	DB_CONFIG  *DB

	APP_ENV       string
	IS_PRODUCTION bool

	DB_HOST     string
	DB_USER     string
	DB_PASSWORD string
	DB_ADMIN    string
	DB_NAME     string
	COLL_USERS  string
)

const (
	ACCESS_TOKEN_NAME  = "access_token"
	REFRESH_TOKEN_NAME = "_refresh_token"
	CSRF_COOKIE_NAME = "_csrf_token"
)

func init() {
	APP_ENV = lib.GetEnv("APP_ENV", "development")
	IS_PRODUCTION := APP_ENV == "production"

	APP_CONFIG = &App{
		Name: "Go Web Server",
		Port: 8000,
	}

	DB_HOST = os.Getenv("DB_HOST_PROD")
	if !IS_PRODUCTION {
		DB_HOST = os.Getenv("DB_HOST_DEV")
	}

	DB_USER = os.Getenv("DB_USER")
	DB_PASSWORD = os.Getenv("DB_PASSWORD")
	DB_ADMIN = os.Getenv("DB_ADMIN")
	DB_NAME = os.Getenv("DB_NAME")
	COLL_USERS = os.Getenv("COLL_USERS")

	DB_CONFIG = &DB{
		Host:     DB_HOST,
		Port:     27017,
		User:     DB_USER,
		Password: DB_PASSWORD,
		AdminDb:  DB_ADMIN,
		DbName:   DB_NAME,
	}
}
