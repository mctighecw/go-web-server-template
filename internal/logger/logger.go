package logger

import (
	"os"

	"go-web-server/internal/config"
	"go-web-server/internal/lib"

	"github.com/google/uuid"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	LOG_FILE_PATH = "./logs/server_log_file.txt"
)

func CreateLogger() *zap.Logger {
	// Env
	isDevelopment := !config.IS_PRODUCTION
	saveLogsToFile := lib.GetEnv("SAVE_LOGS_TO_FILE", "false") == "true"

	// Output
	outputPaths := []string{"stderr"}
	if saveLogsToFile {
		outputPaths = append(outputPaths, LOG_FILE_PATH)
	}

	// Encoder Config
	encoderCfg := zap.NewProductionEncoderConfig()
	if isDevelopment {
		encoderCfg = zap.NewDevelopmentEncoderConfig()
	}
	encoderCfg.TimeKey = "timestamp"
	encoderCfg.EncodeTime = zapcore.ISO8601TimeEncoder

	// Logger Config
	config := zap.Config{
		Level:             zap.NewAtomicLevelAt(zap.InfoLevel),
		Development:       isDevelopment,
		DisableCaller:     false,
		DisableStacktrace: false,
		Sampling:          nil,
		Encoding:          "json",
		EncoderConfig:     encoderCfg,
		OutputPaths:       outputPaths,
		ErrorOutputPaths:  outputPaths,
		InitialFields: map[string]interface{}{
			"log_uuid": uuid.New().String(),
			"pid":      os.Getpid(),
		},
	}

	return zap.Must(config.Build())
}
