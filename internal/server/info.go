package server

// General API Info for Swagger
// @title            Go Web Server Template
// @version          1.0
// @description      This is a web server template using the Go Echo framework.
// @contact.email    mctighecw@gmail.com
// @license.name     MIT
// @license.url      https://gitlab.com/mctighecw/go-web-server-template/-/blob/master/LICENSE.txt
// @host             localhost:8000
// @BasePath         /api
// @Schemes          http https
