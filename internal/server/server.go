package server

import (
	"fmt"

	"go-web-server/internal/config"
	"go-web-server/internal/lib"

	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

func CreateServer() *echo.Echo {
	e := echo.New()

	// Log level
	logLevel := lib.If(config.IS_PRODUCTION, log.ERROR, log.DEBUG)
	e.Logger.SetLevel(logLevel)

	// Middleware
	addMiddleware(e)

	// Routes
	createRoutes(e)

	// Static assets
	e.Static("/static", "static")
	e.File("/favicon.ico", "static/favicon.png")

	// Mode
	mode := fmt.Sprintf("Mode: %s", config.APP_ENV)
	fmt.Println(mode)

	return e
}
