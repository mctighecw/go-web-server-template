package server

import (
	"net/http"

	_ "go-web-server/docs"
	"go-web-server/internal/app/handler"
	"go-web-server/internal/lib"
	"go-web-server/internal/tokens"

	"github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"github.com/swaggo/echo-swagger"
)

func createRoutes(e *echo.Echo) {
	// Env
	APP_ENV := lib.GetEnv("APP_ENV", "development")

	// Create all routes
	e.GET("/", func(c echo.Context) error {
		errMsg := lib.ErrorMessage("Invalid path")
		return c.JSON(http.StatusNotFound, errMsg)
	})

	// Swagger
	if APP_ENV == "development" {
		e.GET("/swagger/*", echoSwagger.WrapHandler)
	}

	// All API routes
	a := e.Group("/api")

	// Unprotected
	a.POST("/login", handler.Login)
	a.POST("/register", handler.CreateUser)
	a.GET("/logout", handler.Logout)
	a.GET("/hello", handler.GetHello)

	// Protected
	a.Use(echojwt.WithConfig(tokens.JWT_CONFIG))
	a.GET("/me", handler.GetMe)
	a.GET("/users", handler.GetAllUsers)
	a.GET("/users/:id", handler.GetUser)
	a.POST("/users/:id", handler.UpdateUser)
	a.DELETE("/users/:id", handler.DeleteUser)
}
