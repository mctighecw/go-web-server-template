package controller

import (
	"go-web-server/internal/model"
)

func ReturnUserData(u model.User) interface{} {
	res := map[string]interface{}{
		"id":         u.ID,
		"name":       u.Name,
		"username":   u.Username,
		"admin":      u.Admin,
		"created_at": u.CreatedAt,
	}
	return res
}

func ReturnAllUsersData(d []model.User) interface{} {
	e := []interface{}{}

	for _, el := range d {
		f := ReturnUserData(el)
		e = append(e, f)
	}

	res := map[string]interface{}{
		"status": "OK",
		"data":   e,
	}
	return res
}
