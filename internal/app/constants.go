package app

const (
	ERROR_USER_DOES_NOT_EXIST         = "User does not exist"
	FORBIDDEN_INSUFFICIENT_PRIVILEGES = "Forbidden - insufficient privileges"
	MISSING_INVALID_TOKEN_MESSAGE     = "Missing or invalid authentication token"
	MISSING_INVALID_CSRF_MESSAGE      = "Missing or invalid csrf token"
)
