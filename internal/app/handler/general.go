package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// @Summary      Hello message
// @Description  Return "Hello World" message.
// @Tags         general
// @Produce      json
// @Success      200 {object} lib.MessageResponse
// @Router       /hello [GET]
func GetHello(c echo.Context) error {
	res := map[string]interface{}{
		"message": "Hello World",
	}
	return c.JSON(http.StatusOK, res)
}
