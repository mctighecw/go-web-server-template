package handler

import (
	"fmt"
	"net/http"

	"go-web-server/internal/config"
	"go-web-server/internal/db"
	"go-web-server/internal/lib"
	"go-web-server/internal/model"
	"go-web-server/internal/tokens"

	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
)

var (
	DbName    = config.DB_NAME
	CollUsers = config.COLL_USERS
)

// @Summary      Log in user
// @Description  Log a user in.
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        username body string true "Username"
// @Param        password body string true "Password"
// @Success      200 {object} lib.StatusResponse
// @Failure      401 {object} lib.StatusResponse
// @Router       /login [POST]
func Login(c echo.Context) error {
	// POST - logs a user in with a username and password
	m := lib.GetEchoMap(c)

	username := lib.ReturnStringValue(m, "username")
	password := lib.ReturnStringValue(m, "password")

	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	res := coll.FindOne(ctx, bson.M{"username": username})
	err := res.Err()

	user := model.User{}
	res.Decode(&user)
	pwValid := lib.CheckPasswordHash(password, user.Password)

	if err != nil || !pwValid {
		r := lib.ReturnError("Unauthorized")
		return c.JSON(http.StatusUnauthorized, r)
	}

	id := user.ID

	tokens.CreateAccessTokenCookie(id, c)
	tokens.CreateRefreshTokenCookie(id, c)

	message := fmt.Sprintf("%s has logged in", username)
	r := lib.ReturnOK(message)

	return c.JSON(http.StatusOK, r)
}

// @Summary      Log out user
// @Description  Log a user out.
// @Tags         auth
// @Produce      json
// @Success      200 {object} lib.StatusResponse
// @Router       /logout [GET]
func Logout(c echo.Context) error {
	// GET - logs a user out by invalidating cookie
	tokens.InvalidateAccessTokenCookie(c)
	r := lib.ReturnOK("User has been logged out")

	return c.JSON(http.StatusOK, r)
}
