package handler

import (
	"net/http"
	"time"

	"go-web-server/internal/app"
	"go-web-server/internal/app/controller"
	"go-web-server/internal/db"
	"go-web-server/internal/lib"
	"go-web-server/internal/model"
	"go-web-server/internal/tokens"

	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// @Summary      Create a new user
// @Description  Create a new user with the required body params.
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        username body string true "Username"
// @Param        name body string true "First and last name"
// @Param        password body string true "Password"
// @Success      200 {object} lib.StatusResponse
// @Failure      400 {object} lib.StatusResponse
// @Failure      409 {object} lib.StatusResponse
// @Failure      500 {object} lib.StatusResponse
// @Router       /register [POST]
func CreateUser(c echo.Context) error {
	// POST - creates a new user and saves to database
	m := lib.GetEchoMap(c)

	_, field1 := m["username"]
	_, field2 := m["name"]
	_, field3 := m["password"]

	if !(field1 && field2 && field3) {
		res := lib.ReturnError("Error creating new user - missing fields")
		return c.JSON(http.StatusBadRequest, res)
	}

	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	username := lib.ReturnStringValue(m, "username")
	findRes := coll.FindOne(ctx, bson.M{"username": username})
	err1 := findRes.Err()

	if err1 == nil {
		res := lib.ReturnError("Error creating new user - username already exists")
		return c.JSON(http.StatusConflict, res)
	}

	name := lib.ReturnStringValue(m, "name")
	password := lib.ReturnStringValue(m, "password")
	passwordHash := lib.HashPassword(password)

	u := model.User{
		ID:        primitive.NewObjectID(),
		Username:  username,
		Name:      name,
		Admin:     false,
		Password:  passwordHash,
		CreatedAt: time.Now(),
	}

	_, err2 := coll.InsertOne(ctx, u)
	lib.LogError(err2)

	if err2 != nil {
		res := lib.ReturnError("Error saving new user data")
		return c.JSON(http.StatusInternalServerError, res)
	}

	res := lib.ReturnOK("New user created successfully")
	return c.JSON(http.StatusOK, res)
}

// @Summary      Get me
// @Description  Return the user's own information (using JWT cookie ID).
// @Tags         users
// @Produce      json
// @Success      200 {object} lib.UserData
// @Failure      401 {object} lib.StatusResponse
// @Failure      404 {object} lib.StatusResponse
// @Failure      500 {object} lib.StatusResponse
// @Router       /me [GET]
func GetMe(c echo.Context) error {
	// GET - returns user info based on JWT cookie ID
	userId, err1 := tokens.ReadAccessTokenCookie(c)
	if err1 != nil {
		res := lib.ReturnError("Error reading cookie")
		return c.JSON(http.StatusInternalServerError, res)
	}

	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	objID, _ := primitive.ObjectIDFromHex(userId)
	res := coll.FindOne(ctx, bson.M{"_id": objID})
	err2 := res.Err()

	if err2 != nil {
		res := lib.ReturnError(app.ERROR_USER_DOES_NOT_EXIST)
		return c.JSON(http.StatusNotFound, res)
	}

	u := model.User{}
	res.Decode(&u)

	userInfo := controller.ReturnUserData(u)
	return c.JSON(http.StatusOK, userInfo)
}

// @Summary      List users
// @Description  Return information for all users (admin status required).
// @Tags         users
// @Produce      json
// @Success      200 {array} lib.UserData
// @Failure      401 {object} lib.StatusResponse
// @Failure      403 {object} lib.StatusResponse
// @Router       /users [GET]
func GetAllUsers(c echo.Context) error {
	// GET - returns all users' information

	// Admins only
	isAdmin := tokens.CheckIfAdmin(c)
	if !isAdmin {
		r := lib.ReturnError(app.FORBIDDEN_INSUFFICIENT_PRIVILEGES)
		return c.JSON(http.StatusForbidden, r)
	}

	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	u := model.User{}
	res := []model.User{}
	cursor, err := coll.Find(ctx, bson.M{})
	lib.LogError(err)

	for cursor.Next(ctx) {
		cursor.Decode(&u)
		res = append(res, u)
	}

	d := controller.ReturnAllUsersData(res)
	return c.JSON(http.StatusOK, d)
}

// @Summary      Get user
// @Description  Return information for a user (admin status required).
// @Tags         users
// @Produce      json
// @Param        id path string true "User ID"
// @Success      200 {object} lib.UserData
// @Failure      401 {object} lib.StatusResponse
// @Failure      403 {object} lib.StatusResponse
// @Failure      404 {object} lib.StatusResponse
// @Router       /users/:id [GET]
func GetUser(c echo.Context) error {
	// GET - returns a user's information

	// Admins only
	isAdmin := tokens.CheckIfAdmin(c)
	if !isAdmin {
		r := lib.ReturnError(app.FORBIDDEN_INSUFFICIENT_PRIVILEGES)
		return c.JSON(http.StatusForbidden, r)
	}

	userId := c.Param("id")

	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	objID, _ := primitive.ObjectIDFromHex(userId)
	res := coll.FindOne(ctx, bson.M{"_id": objID})
	err2 := res.Err()

	if err2 != nil {
		res := lib.ReturnError(app.ERROR_USER_DOES_NOT_EXIST)
		return c.JSON(http.StatusNotFound, res)
	}

	u := model.User{}
	res.Decode(&u)

	userInfo := controller.ReturnUserData(u)
	return c.JSON(http.StatusOK, userInfo)
}

// @Summary      Update user
// @Description  Update an existing user with the body params (admin status required).
// @Tags         users
// @Accept       json
// @Produce      json
// @Param        id path string true "User ID"
// @Param        username body string false "Username"
// @Param        name body string false "First and last name"
// @Success      200 {object} lib.StatusResponse
// @Failure      401 {object} lib.StatusResponse
// @Failure      403 {object} lib.StatusResponse
// @Failure      404 {object} lib.StatusResponse
// @Failure      500 {object} lib.StatusResponse
// @Router       /users/:id [POST]
func UpdateUser(c echo.Context) error {
	// POST - updates a user's information in database

	// Admins only
	isAdmin := tokens.CheckIfAdmin(c)
	if !isAdmin {
		r := lib.ReturnError(app.FORBIDDEN_INSUFFICIENT_PRIVILEGES)
		return c.JSON(http.StatusForbidden, r)
	}

	userId := c.Param("id")

	m := lib.GetEchoMap(c)
	_, field1 := m["username"]
	_, field2 := m["name"]

	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	objID, _ := primitive.ObjectIDFromHex(userId)
	res := coll.FindOne(ctx, bson.M{"_id": objID})
	err1 := res.Err()

	if err1 != nil {
		res := lib.ReturnError(app.ERROR_USER_DOES_NOT_EXIST)
		return c.JSON(http.StatusNotFound, res)
	}

	user := model.User{}
	res.Decode(&user)

	// TODO: fix warnings
	_, err2 := coll.UpdateOne(ctx, bson.M{"_id": objID}, bson.D{{"$set", bson.D{
		{"username", lib.If(field1, lib.ReturnStringValue(m, "username"), user.Username)},
		{"name", lib.If(field2, lib.ReturnStringValue(m, "name"), user.Name)},
	}}})
	if err2 != nil {
		msg := lib.ReturnError("Error updating user")
		return c.JSON(http.StatusInternalServerError, msg)
	}

	msg := lib.ReturnOK("User updated successfully")
	return c.JSON(http.StatusOK, msg)
}

// @Summary      Delete user
// @Description  Delete an existing user (admin status required).
// @Tags         users
// @Produce      json
// @Param        id path string true "User ID"
// @Success      200 {object} lib.StatusResponse
// @Failure      401 {object} lib.StatusResponse
// @Failure      403 {object} lib.StatusResponse
// @Failure      404 {object} lib.StatusResponse
// @Router       /users/:id [DELETE]
func DeleteUser(c echo.Context) error {
	// DELETE - deletes a single user from database

	// Admins only
	isAdmin := tokens.CheckIfAdmin(c)
	if !isAdmin {
		r := lib.ReturnError(app.FORBIDDEN_INSUFFICIENT_PRIVILEGES)
		return c.JSON(http.StatusForbidden, r)
	}

	userId := c.Param("id")

	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	objID, _ := primitive.ObjectIDFromHex(userId)
	_, err2 := coll.DeleteOne(ctx, bson.M{"_id": objID})

	if err2 != nil {
		res := lib.ReturnError(app.ERROR_USER_DOES_NOT_EXIST)
		return c.JSON(http.StatusNotFound, res)
	}

	msg := lib.ReturnOK("User deleted successfully")
	return c.JSON(http.StatusOK, msg)
}
