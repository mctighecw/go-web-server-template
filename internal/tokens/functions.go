package tokens

import (
	"fmt"
	"net/http"
	"time"

	"go-web-server/internal/config"
	"go-web-server/internal/db"
	"go-web-server/internal/model"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var (
	DbName    = config.DB_NAME
	CollUsers = config.COLL_USERS

	AccessTokenName  = config.ACCESS_TOKEN_NAME
	RefreshTokenName = config.REFRESH_TOKEN_NAME
)

func createJwtClaims(id primitive.ObjectID, expiresAt time.Time) *JwtCustomClaims {
	return &JwtCustomClaims{
		ID: id,
		RegisteredClaims: jwt.RegisteredClaims{
			NotBefore: jwt.NewNumericDate(time.Now()),
			ExpiresAt: jwt.NewNumericDate(expiresAt),
		},
	}
}

func createCookie(token string, name string, expiration time.Time) *http.Cookie {
	// Set to secure in production
	isSecure := config.IS_PRODUCTION

	cookie := new(http.Cookie)
	cookie.Name = name
	cookie.Value = token
	cookie.Expires = expiration
	cookie.Path = "/"
	cookie.HttpOnly = true
	cookie.Secure = isSecure

	return cookie
}

func CreateAccessTokenCookie(id primitive.ObjectID, c echo.Context) {
	// Expires in 15 minutes
	expiration := 15 * time.Minute
	cookieExpiration := time.Now().Add(expiration)

	claims := createJwtClaims(id, cookieExpiration)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tss, _ := token.SignedString([]byte(JWT_SECRET))
	ac := createCookie(tss, AccessTokenName, cookieExpiration)

	c.SetCookie(ac)
}

func CreateRefreshTokenCookie(id primitive.ObjectID, c echo.Context) {
	// Expires in 7 days
	expiration := 7 * 24 * time.Hour
	cookieExpiration := time.Now().Add(expiration)

	claims := createJwtClaims(id, cookieExpiration)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tss, _ := token.SignedString([]byte(JWT_SECRET))
	rc := createCookie(tss, RefreshTokenName, cookieExpiration)

	c.SetCookie(rc)
}

func InvalidateAccessTokenCookie(c echo.Context) {
	expiration := -24 * time.Hour
	cookieExpiration := time.Now().Add(expiration)

	claims := createJwtClaims(primitive.NilObjectID, cookieExpiration)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tss, _ := token.SignedString([]byte(JWT_SECRET))
	ac := createCookie(tss, AccessTokenName, cookieExpiration)
	rc := createCookie(tss, RefreshTokenName, cookieExpiration)

	c.SetCookie(ac)
	c.SetCookie(rc)
}

func ReadAccessTokenCookie(c echo.Context) (string, error) {
	tokenString, err1 := c.Cookie(AccessTokenName)
	if err1 != nil {
		return "", err1
	}

	claims := &JwtCustomClaims{}
	_, err2 := jwt.ParseWithClaims(tokenString.Value, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(JWT_SECRET), nil
	})
	if err2 != nil {
		return "", err2
	}

	userId := claims.ID.Hex()

	return userId, nil
}

func checkRefreshTokenCookie(c echo.Context) error {
	// Check refresh token
	tokenString, err1 := c.Cookie(RefreshTokenName)
	if err1 != nil {
		return err1
	}

	claims := &JwtCustomClaims{}
	_, err2 := jwt.ParseWithClaims(tokenString.Value, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(JWT_SECRET), nil
	})
	if err2 != nil {
		return err2
	}

	// Issue new access token
	CreateAccessTokenCookie(claims.ID, c)

	return nil
}

func checkUserTokens(c echo.Context) error {
	tokenString, err1 := c.Cookie(AccessTokenName)
	if err1 != nil {
		return err1
	}

	claims := &JwtCustomClaims{}
	_, err2 := jwt.ParseWithClaims(tokenString.Value, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(JWT_SECRET), nil
	})
	if err2 != nil {
		return err2
	}

	acExp := claims.ExpiresAt.Unix()
	t5min := time.Now().Add(time.Minute * time.Duration(5)).Unix()

	// Issue new access token if current token expires in under 5 minutes
	if t5min > acExp {
		fmt.Println("Access token expiring in under 5 min")
		checkRefreshTokenCookie(c)
	}

	return nil

}

func CheckIfAdmin(c echo.Context) bool {
	// Check if user has admin privileges
	userId, err := ReadAccessTokenCookie(c)
	if err != nil {
		return false
	}

	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	objID, _ := primitive.ObjectIDFromHex(userId)
	findRes := coll.FindOne(ctx, bson.M{"_id": objID})
	if findRes == nil {
		return false
	}

	u := model.User{}
	findRes.Decode(&u)

	return u.Admin
}
