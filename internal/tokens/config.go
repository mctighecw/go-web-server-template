package tokens

import (
	"go-web-server/internal/config"
	"go-web-server/internal/lib"

	"github.com/golang-jwt/jwt/v4"
	"github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4/middleware"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var (
	JWT_SECRET  string
	JWT_CONFIG  echojwt.Config
	CSRF_CONFIG middleware.CSRFConfig

	CSRFCookieName = config.CSRF_COOKIE_NAME
)

type JwtCustomClaims struct {
	ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
	jwt.RegisteredClaims
}

func init() {
	APP_ENV := lib.GetEnv("APP_ENV", "development")
	JWT_SECRET = lib.GetEnv("JWT_SECRET", "abc123")

	JWT_CONFIG = echojwt.Config{
		SigningKey:     []byte(JWT_SECRET),
		TokenLookup:    "cookie:" + AccessTokenName,
		SuccessHandler: customJwtSuccessHandler,
		ErrorHandler:   customJwtErrorHandler,
	}

	CSRF_CONFIG = middleware.CSRFConfig{
		TokenLookup:    "cookie:" + CSRFCookieName,
		CookieName:     CSRFCookieName,
		CookiePath:     "/",
		CookieHTTPOnly: true,
		CookieSecure:   APP_ENV == "production",
		ErrorHandler:   customCsrfErrorHandler,
	}
}
