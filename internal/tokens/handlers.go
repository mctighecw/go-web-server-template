package tokens

import (
	"net/http"

	"go-web-server/internal/app"
	"go-web-server/internal/lib"

	"github.com/labstack/echo/v4"
)

func customJwtSuccessHandler(c echo.Context) {
	err := checkUserTokens(c)
	if err != nil {
		lib.LogError(err)
	}
}

func customJwtErrorHandler(c echo.Context, err error) error {
	msg := lib.ErrorMessage(app.MISSING_INVALID_TOKEN_MESSAGE)
	return c.JSON(http.StatusUnauthorized, msg)
}

func customCsrfErrorHandler(err error, c echo.Context) error {
	msg := lib.ErrorMessage(app.MISSING_INVALID_CSRF_MESSAGE)
	return c.JSON(http.StatusUnauthorized, msg)
}
