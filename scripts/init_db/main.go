package main

import (
	"fmt"
	"time"

	"go-web-server/internal/config"
	"go-web-server/internal/db"
	"go-web-server/internal/lib"
	"go-web-server/internal/model"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

var (
	DbName    = config.DB_CONFIG.DbName
	CollUsers = config.COLL_USERS

	seeds = []interface{}{}

	u1 = model.User{
		ID:        primitive.NewObjectID(),
		Username:  "jsmith",
		Name:      "John Smith",
		Admin:     true,
		Password:  lib.HashPassword("john1"),
		CreatedAt: time.Now(),
	}

	u2 = model.User{
		ID:        primitive.NewObjectID(),
		Username:  "sjones",
		Name:      "Sam Jones",
		Admin:     false,
		Password:  lib.HashPassword("sam1"),
		CreatedAt: time.Now(),
	}
)

func main() {
	client, ctx := db.DbConnect()
	db := client.Database(DbName)
	coll := db.Collection(CollUsers)

	seeds = append(seeds, u1, u2)
	result, err := coll.InsertMany(ctx, seeds)

	lib.LogError(err)
	fmt.Println(result)
}
