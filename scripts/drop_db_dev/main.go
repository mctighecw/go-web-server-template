package main

import (
	"fmt"

	"go-web-server/internal/config"
	"go-web-server/internal/db"

	"go.mongodb.org/mongo-driver/bson"
)

var (
	DbName    = config.DB_CONFIG.DbName
	CollUsers = config.COLL_USERS
)

func main() {
	if !config.IS_PRODUCTION {
		client, ctx := db.DbConnect()
		db := client.Database(DbName)
		coll1 := db.Collection(CollUsers)

		f := bson.M{}
		_, err1 := coll1.DeleteMany(ctx, f)

		if err1 != nil {
			m := fmt.Sprintf("Error dropping database %s", DbName)
			fmt.Println(m)
		} else {
			fmt.Println("Database has been dropped")
		}
	} else {
		fmt.Println("Can only drop database in development environment")
	}
}
