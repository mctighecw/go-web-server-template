# Makefile
# (%) make dev-up

SERVER_CONTAINER  	:= go-web-server
DB_CONTAINER		:= go-web-server-mongodb

# development
dev-up:
	docker-compose up -d --build

dev-down:
	docker-compose down

# production
prod-up:
	docker-compose -f docker-compose-prod.yml up -d --build

prod-down:
	docker-compose -f docker-compose-prod.yml down

# db
db-setup:
	docker exec -it ${DB_CONTAINER} mongo

db-enter:
	docker exec -it ${DB_CONTAINER} mongo -u admin --authenticationDatabase admin -p

db-init:
	docker exec -it ${SERVER_CONTAINER} go run /opt/app/scripts/init_db/main.go

db-drop:
	docker exec -it ${SERVER_CONTAINER} go run /opt/app/scripts/drop_db_dev/main.go

db-reset:
	make db-drop
	make db-init

# misc
docker-logs:
	docker logs -f ${SERVER_CONTAINER}

ps:
	docker ps -a
